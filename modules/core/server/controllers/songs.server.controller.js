'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');


/**
 * Extend user's controller
 */
module.exports = _.extend(
  require('./songs/songs.manager.server.controller')
);

