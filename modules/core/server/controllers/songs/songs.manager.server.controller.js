'use strict';
/**
 * Module dependencies.
 */
var _ = require('lodash'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  Song = mongoose.model('Song');

/**
 * Store a Song
 */
exports.storeSong = function (req, res) {

  var song = new Song(req.body);

  song.provider = 'local';

  song.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
   		res.json({res:'Stored!'});   
    }
  });
};

/**
 * Get all Songs
 */
exports.getallSong = function (req, res) {

	Song.find({},function (err, songs) { 
	    if (err) {
	      return res.status(400).send({
	        message: errorHandler.getErrorMessage(err)
	      });
	    } else {
	   		res.json(songs);   
	    }
  	});
};