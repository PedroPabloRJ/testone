'use strict';


module.exports = function (app) {
  // Root routing
  var core = require('../controllers/core.server.controller');
  
  //Songs routing
  var songs = require('../controllers/songs.server.controller');

  // Define error pages
  app.route('/server-error').get(core.renderServerError);

  // Return a 404 for all undefined api, module or lib routes
  app.route('/:url(api|modules|lib)/*').get(core.renderNotFound);

  //Songs routes
  app.route('/store-song').post(songs.storeSong);
  app.route('/getall-song').post(songs.getallSong);

  // Define application route
  app.route('/*').get(core.renderIndex);

};
