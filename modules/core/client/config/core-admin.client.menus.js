'use strict';

angular.module('core.admin').run(['Menus',
  function (Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Favorites Songs',
      state: 'fav-songs',
      type: 'strict',
      roles: ['*']
    });
  }
]);
