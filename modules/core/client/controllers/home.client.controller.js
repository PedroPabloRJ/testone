'use strict';

angular.module('core').controller('HomeController', ['$scope','$http', '$timeout',
	function($scope,$http,$timeout){
		//Variable initialization
		$scope.song = {};
		$scope.messaje = false;

		//Methods
		$scope.storeSong = function(){
			$http.post('/store-song',$scope.song).success(function(res){
				$scope.messaje = true;
				$scope.song = {};
				$timeout(function(){
					$scope.messaje = false;
				}, 1000);
    		});
    	};
	}
]);

angular.module('core').controller('FavController', ['$scope','$http',
	function($scope,$http){
		//Variable initialization
		$scope.songs = {};

		//Methods
		$http.post('/getall-song').success(function(res){
			$scope.songs = res;
    	}).error(function(err){
    		console.log(err);
    	});
	}
]);
